import datetime
from datetime import date
from django.utils import timezone
from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from app.time_utils import prettydate
from django.contrib.auth.models import AbstractUser, Group, Permission
from pprint import pprint

class Servicio(models.Model):
    nombre = models.CharField(max_length=200, null=False, blank=False)
    
    def __str__(self):
        return self.nombre

    def contar_pendientes(self):
        hoy = timezone.now().date()
        return Turno.objects.filter(estado__in=[1,3]).filter(servicio=self).filter(fecha_hora_llegada__date=hoy).count()

    def contar_atendidos(self):
        hoy = timezone.now().date()
        return Turno.objects.filter(estado__in=[2,4]).filter(servicio=self).filter(fecha_hora_llegada__date=hoy).count()


class BocaAtencion(models.Model):
    nombre = models.CharField(max_length=200, null=False, blank=False)
    activo = models.BooleanField(default=True, null=False)
    servicio = models.ForeignKey(Servicio, null=True, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombre
    
    class Meta:
        verbose_name = "Boca de Atención"
        verbose_name_plural = "Bocas de Atención"


class Condicion(models.Model):
    #Constantes
    PRIORIDAD_CHOICES = (
        (1, "Alta"),
        (2, "Media"),
        (3, "Baja"),
    )
    #Campos de la BD
    nombre = models.CharField(max_length=200, null=False, blank=False)
    prioridad = models.PositiveSmallIntegerField(choices=PRIORIDAD_CHOICES, default=3)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    ultima_actualizacion = models.DateTimeField(auto_now=True)
    
    list_display = ('nombre', 'prioridad')
    @admin.display(ordering='prioridad', description='Prioridad')

    def get_prioridad(self):
        return dict(Condicion.PRIORIDAD_CHOICES).get(self.prioridad)
    
    def __str__(self):
        return self.nombre
    
    class Meta:
        verbose_name = "Condición"
        verbose_name_plural = "Condiciones"


class Cliente(models.Model):
    documento = models.CharField(verbose_name="Número de Documento", max_length=50, null=False, blank=False, unique=True)
    nombres = models.CharField(verbose_name="Nombre/s", max_length=100, null=False, blank=False)
    apellidos = models.CharField(verbose_name="Apellido/s", max_length=100, null=False, blank=False)
    GENDER_CHOICES = (
        ('Desconocido', 'Desconocido'),
        ('Femenino', 'Femenino'),
        ('Masculino', 'Masculino'),
    )
    genero = models.CharField(verbose_name="Género", max_length=12, choices=GENDER_CHOICES, default="Desconocido")
    telefono = models.CharField(verbose_name="Teléfono/s", max_length=50, null=True, default=None, blank=True)
    direccion = models.CharField(verbose_name="Dirección/es", max_length=200, null=True, default=None, blank=True)
    fecha_nacimiento = models.DateField(verbose_name="Fecha de Nacimiento", null=True, default=None, blank=True)
    condicion = models.ForeignKey(Condicion, null=True, on_delete=models.RESTRICT, default=None, blank=True, verbose_name="Condición/Tratamiento")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    ultima_actualizacion = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.nombres} {self.apellidos}"

    @property
    def nombre_completo(self):
        return f"{self.nombres} {self.apellidos}"

    @property
    def nombre_completo_con_edad(self):
        detalle = ""
        if self.fecha_nacimiento:
            hoy = date.today()
            edad = hoy.year - self.fecha_nacimiento.year -((hoy.month, hoy.day) < (self.fecha_nacimiento.month, self.fecha_nacimiento.day))
            detalle = f" ({edad} AÑOS)"
        return f"{self.nombres} {self.apellidos} {detalle}"


class Turno(models.Model):
    ESTADO_CHOICES = (
        (1, "Llamada"),
        (2, "Atendiendo"),
        (3, "Pendiente"),
        (4, "Finalizado"),
        (5, "Cancelado"),
    )
    ESTADO_COLORS = (
        (1, "info"),
        (2, "warning"),
        (3, "danger"),
        (4, "success"),
        (5, "secondary"),
    )
    cliente = models.ForeignKey(Cliente, null=False, blank=False, on_delete=models.CASCADE, verbose_name="Cliente")
    servicio = models.ForeignKey(Servicio, null=True, blank=True, default=None, on_delete=models.CASCADE, verbose_name="Servicio")
    usuario_asesor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_asesor', null=True, blank=True, default=None)
    usuario_atencion = models.ForeignKey(User, on_delete=models.CASCADE, related_name='usuario_atencion', null=True, blank=True, default=None)

    boca_atencion = models.ForeignKey(BocaAtencion, null=True, blank=True, default=None, on_delete=models.RESTRICT, verbose_name="Boca de Atención")
    condicion = models.ForeignKey(Condicion, null=True, default=None, blank=True, on_delete=models.RESTRICT, verbose_name="Condición/Tratamiento")
    fecha_hora_llegada = models.DateTimeField(auto_now_add=True)
    fecha_hora_llamada = models.DateTimeField(null=True, default=None, blank=True)
    fecha_hora_atencion = models.DateTimeField(null=True, default=None, blank=True)
    fecha_hora_finalizacion = models.DateTimeField(null=True, default=None, blank=True)
    estado = models.PositiveSmallIntegerField(choices=ESTADO_CHOICES, default=3)
    exclude = ('fecha_hora_llegada')

    def get_estado(self):
        return dict(Turno.ESTADO_CHOICES).get(self.estado)

    def get_estado_color(self):
        return dict(Turno.ESTADO_COLORS).get(self.estado)
    
    def __str__(self):
        return f"{self.cliente}"

    def tiempo_en_espera(self):
        return prettydate(self.fecha_hora_llegada)

    def tiempo_de_llamada(self):
        return prettydate(self.fecha_hora_llamada)

    def contar_turnos(self, estado_id, servicio):
        hoy = timezone.now().date()
        return Turno.objects.filter(estado=estado_id).filter(servicio=servicio).filter(fecha_hora_llegada__date=hoy).count()




class UsarioBocaAtencion(models.Model):
    # Se agrega boca de atención como relacion uno a uno
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    boca_atencion = models.OneToOneField(
        BocaAtencion,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=None,
        verbose_name='Boca de Atención'
    )

    def __str__(self):
        return f"{self.boca_atencion.nombre}"

    def contar_atendidos_por_usuario(self):
        hoy = timezone.now().date()
        return Turno.objects.filter(estado__in=[2,4]).filter(boca_atencion=self.boca_atencion).filter(fecha_hora_llegada__date=hoy).count()

    def get_turno_actual(self):
        hoy = timezone.now().date()
        return Turno.objects.exclude(estado__gt=2).filter(boca_atencion=self.boca_atencion).filter(fecha_hora_llegada__date=hoy).first()
