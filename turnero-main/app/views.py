from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from formtools.wizard.views import SessionWizardView
from app.forms import SeleccionarCliente,SeleccionarCondicion
from django.contrib import messages
from app.models import Turno, BocaAtencion, UsarioBocaAtencion 
from django.contrib.auth.models import User
from django.db.models import F
from django.utils import timezone
from django.shortcuts import redirect
from datetime import date
from datetime import datetime
from pprint import pprint


def menuPrincipal(request):
    return render(request, './index.html')

class ReservarTurnoWizard(SessionWizardView):
    form_list = [SeleccionarCliente, SeleccionarCondicion]
    template_name = 'reserva.html'

    def get_form(self, step=None, data=None, files=None):
        form = super(ReservarTurnoWizard, self).get_form(step, data, files)
        if step is None:
            step = self.steps.current
        if step == '1':
            cliente = self.get_cleaned_data_for_step('0')['documento']
            form.fields['cliente_nombre'].initial = cliente.nombre_completo_con_edad
            form.fields['cliente_id'].initial = cliente.id
            form.fields['condicion_id'].initial = cliente.condicion_id
        return form

    def done(self, form_list, **kwargs):
        cliente = self.get_cleaned_data_for_step('0')['documento']
        condicion = self.get_cleaned_data_for_step('1')['condicion_id']
        servicio = self.get_cleaned_data_for_step('1')['servicio_id']
        usuario_asesor = self.request.user
        self.instance_dict = {}
        self.storage.reset()
        
        try:
            turno = Turno(cliente=cliente, condicion=condicion, servicio=servicio, usuario_asesor=usuario_asesor)
            turno.save()
            messages.success(self.request, "Turno registrado Exitosamente!!!")
        except Exception as e:
            messages.warning(self.request, "No se pudo guardar el Registro por favor intente nuevamente!!!")
            pprint(e)

        return self.render_goto_step('0')
        

def atencionClientes(request):
    hoy = timezone.now().date()
    u = User.objects.get(id=request.user.id)
    uba = UsarioBocaAtencion.objects.get(user=request.user)

    #Verifica que el Usuario tenga asignada una Boca de Atención
    if not uba:
        messages.error(request, 'No tiene asignada ninguna Boca de Atención.')
        return redirect('menu_principal')

    #Turno actual
    turno_actual = uba.get_turno_actual()

    #Listado de Turnos
    turnos = Turno.objects.annotate(
        prioridad=F('condicion__prioridad')
    ).filter(fecha_hora_llegada__date=hoy).filter(servicio=uba.boca_atencion.servicio).order_by('estado','prioridad','fecha_hora_llegada')
    pendientes = uba.boca_atencion.servicio.contar_pendientes()
    atendidos = uba.boca_atencion.servicio.contar_atendidos()
    atendidos_por_usuario = uba.contar_atendidos_por_usuario()
    participacion = round((atendidos_por_usuario / atendidos) * 100) if atendidos != 0 else 0

    params = {
        'turnos':turnos, 
        'turno_actual':turno_actual, 
        'boca_atencion':uba.boca_atencion,
        'pendientes':pendientes, 
        'atendidos':atendidos,
        'atendidos_por_usuario':atendidos_por_usuario,
        'participacion':participacion,

    }
    return render(request, './atencion.html', params)


def llamarSiguiente(request):
    hoy = timezone.now().date()
    u = User.objects.get(id=request.user.id)
    uba = UsarioBocaAtencion.objects.get(user=request.user)

    #Verifica que el Usuario tenga asignada una Boca de Atención
    if not uba:
        messages.error(request, 'No tiene asignada ninguna Boca de Atención.')
        return redirect('menu_principal')

    #Turno actual
    turno_actual = uba.get_turno_actual()

    #Si tiene algún turno asignado, bloquea el llamado a esta funcion
    if turno_actual:
        messages.warning(request, 'Ya tiene asignado un Cliente, por favor concluya la atención del mismo para llamar al siguiente.')
        return redirect('atencion_clientes')

    #Obtiene el primer registro que cumpla con los requisitos de prioridad y horario de llegada
    turno = Turno.objects.annotate(
        prioridad=F('condicion__prioridad')
    ).filter(fecha_hora_llegada__date=hoy).filter(estado=3).filter(servicio=uba.boca_atencion.servicio).order_by('prioridad','fecha_hora_llegada').first()
    
    if turno:
        #actualiza los campos relacionados con el Usuario y la Boca de Atención
        turno.estado = 1
        turno.usuario_atencion = request.user
        turno.boca_atencion = uba.boca_atencion
        turno.fecha_hora_llamada = datetime.now()
        try:
            turno.save()
            messages.success(request, "Llamando al Cliente!")
        except Exception as e:
            messages.error(request, "No se pudo registrar el llamado, por favor intente nuevamente!!!")
            pprint(e)
    else:
        messages.warning(request, "No se encontró ningún Cliente en Espera!")

    return redirect('atencion_clientes')


def atenderTurno(request):
    u = User.objects.get(id=request.user.id)
    uba = UsarioBocaAtencion.objects.get(user=request.user)

    #Verifica que el Usuario tenga asignada una Boca de Atención
    if not uba:
        messages.error(request, 'No tiene asignada ninguna Boca de Atención.')
        return redirect('menu_principal')

    #Turno actual
    turno_actual = uba.get_turno_actual()

    #Si no encuentra el Turno Actual
    if not turno_actual:
        messages.warning(request, "Turno Actual inválido, o ya fue Atendido!")
        return redirect('atencion_clientes')

    #Si tiene algún turno asignado, bloquea el llamado a esta funcion
    if turno_actual.estado != 1:
        messages.error(request, 'El Turno actual ya se encuentra en estado de Atención!')
        return redirect('atencion_clientes')
    
    
    #actualiza los campos relacionados con la Atención del Cliente
    turno_actual.estado = 2    
    turno_actual.fecha_hora_atencion = datetime.now()
    try:
        turno_actual.save()
        messages.success(request, "Se Registró exitosamente el Inició de la Atención!")
    except Exception as e:
        messages.error(request, "No se pudo registrar el Inicio de la Atención, por favor intente nuevamente!!!")
        pprint(e)
    
    return redirect('atencion_clientes')


def cancelarTurno(request):
    u = User.objects.get(id=request.user.id)
    uba = UsarioBocaAtencion.objects.get(user=request.user)

    #Verifica que el Usuario tenga asignada una Boca de Atención
    if not uba:
        messages.error(request, 'No tiene asignada ninguna Boca de Atención.')
        return redirect('menu_principal')

    #Turno actual
    turno_actual = uba.get_turno_actual()

    #Si no encuentra al Turno Actual
    if not turno_actual:
        messages.warning(request, "Turno Actual inválido, o ya fue Atendido!")
        return redirect('atencion_clientes')

    #Si tiene algún turno asignado, bloquea el llamado a esta funcion
    if turno_actual.estado != 1:
        messages.error(request, 'El Turno actual ya se encuentra en estado de Atención!')
        return redirect('atencion_clientes')
    
    
    #actualiza los campos relacionados con la Atención del Cliente
    turno_actual.estado = 5
    turno_actual.fecha_hora_atencion = datetime.now()
    try:
        turno_actual.save()
        messages.success(request, "Cancelación de Turno Exitosa!")
    except Exception as e:
        messages.error(request, "No se pudo Cancelar el Turno del Cliente, por favor intente nuevamente!!!")
        pprint(e)
    
    return redirect('atencion_clientes')


def finalizarTurno(request):
    u = User.objects.get(id=request.user.id)
    uba = UsarioBocaAtencion.objects.get(user=request.user)

    #Verifica que el Usuario tenga asignada una Boca de Atención
    if not uba:
        messages.error(request, 'No tiene asignada ninguna Boca de Atención.')
        return redirect('menu_principal')

    #Turno actual
    turno_actual = uba.get_turno_actual()

    #Si no encuentra el Turno Actual
    if not turno_actual:
        messages.warning(request, "Turno Actual inválido, o ya fue Finalizado!")
        return redirect('atencion_clientes')

    #Si tiene algún turno asignado, bloquea el llamado a esta funcion
    if turno_actual.estado != 2:
        messages.error(request, 'El Turno actual ya fue Finalizado o Cancelado!')
        return redirect('atencion_clientes')
    
    
    #actualiza los campos relacionados con la Atención del Cliente
    turno_actual.estado = 4
    turno_actual.fecha_hora_finalizacion = datetime.now()
    try:
        turno_actual.save()
        messages.success(request, "Finalización del Turno Actual registrado!")
    except Exception as e:
        messages.error(request, "No se pudo registrar la Finalización del Turno Actual, por favor intente nuevamente!!!")
        pprint(e)
    
    return redirect('atencion_clientes')


def pantalla(request):
    hoy = timezone.now().date()
    ahora = datetime.now()

    #Listado de Turnos
    turnos = Turno.objects.annotate(
        prioridad=F('condicion__prioridad')
    ).filter(fecha_hora_llamada__date=hoy).exclude(estado=3).order_by('estado','prioridad','fecha_hora_llamada')[:10]

    params = {
        'turnos': turnos,
        'hora_actual' : ahora.strftime('%I:%M %p')
    }
    return render(request, './pantalla.html', params)