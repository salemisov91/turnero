from django.urls import path
from . import views


urlpatterns = [
    path('', views.menuPrincipal, name='menu_principal'),
    path('pantalla', views.pantalla, name='pantalla'),
]