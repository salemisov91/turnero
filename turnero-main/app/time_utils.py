import datetime
from django.utils import timezone

def prettydate(d):
    if d is not None:
        diff = timezone.now() - d
        s = diff.seconds
        if diff.days > 30 or diff.days < 0:
            return 'Desde {}'.format(d.strftime('%d/%m/%Y'))
        elif diff.days == 1:
            return 'Hace un día'
        elif diff.days > 1:
            return 'Hace {} días'.format(diff.days)
        elif s <= 1:
            return 'Justo ahora'
        elif s < 60:
            return 'Hace {} segundos'.format(s)
        elif s < 120:
            return 'Hace un minuto'
        elif s < 3600:
            return 'Hace {} minutos'.format(round(s/60))
        elif s < 7200:
            return 'Hace una hora'
        else:
            return 'Hace {} horas'.format(round(s/3600))
    else:
        return None